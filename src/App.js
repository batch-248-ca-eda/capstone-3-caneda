import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom'
import { Routes, Route } from 'react-router-dom'
import AppNavbar from './components/AppNavbar';
import './App.css';
import { useState, useEffect } from 'react'
import { UserProvider } from './UserContext'

import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Products from './pages/Products';
import Logout from './pages/Logout';
import Cart from './pages/Cart';
import Dash from './components/Dash';
import Orders from './pages/Orders';
import AddProduct from './components/AddProduct';
import EditProduct from './components/EditProduct';
import ProductView from './components/ProductView';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(()=>{

    fetch('https://capstone-2-caneda.onrender.com/users/details',{
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data=>{

      console.log(data);

      if (typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  },[])

  return (
    
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container fluid id="back">
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/products" element={<Products/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/admin" element={<Dash/>}/>
            <Route path="/addProduct" element={<AddProduct/>} />
            <Route path="/editProduct/:productId" element={<EditProduct/>} />
            <Route path="/products/:productId" element={<ProductView/>}/>
            <Route path="/carts/:userId" element={<Cart/>}/>
            <Route path="/orders/:userId" element={<Orders/>}/>

          </Routes>
        </Container>
      </Router>  
    </UserProvider>

  );
}

export default App;
