import { Form, Button } from 'react-bootstrap';
import img from '../images/regImgTop.jpg';
import { useState, useEffect, useContext } from 'react';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';

export default function Register() {

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	const [isActive, setIsActive] = useState(false);

	function regUser (e) {

		e.preventDefault();

		fetch('https://capstone-2-caneda.onrender.com/users/checkEmailExists',{
			method: 'POST',
			headers:{
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)

			if(data === true){

			    Swal.fire({
			    title: "Email Exists!",
			    type: "error",
			    text: "User another email to register"
			  })

			}else{

			  fetch(`https://capstone-2-caneda.onrender.com/users/register`,{
			    method:"POST",
			    headers:{
			      'Content-Type':'application/json'
			    },
			    body: JSON.stringify({
			      firstName: firstName,
			      lastName: lastName,
			      email: email,
			      mobileNo: mobileNo,
			      password: password1
			    })
			  })
			  .then(res=>res.json())
			  .then(data=>{		

				if(data===true){

				  setFirstName("");
				  setLastName("");
				  setEmail("");
				  setMobileNo("");
				  setPassword1("");
				  setPassword2("");

				  Swal.fire({
				    title: "Registered successfully!",
				    type: "success",
				    text: "Thank you for registering"
				  })

				  navigate("/login")

				} else {

					Swal.fire({
						title: "Something went wrong!",
					    icon: "error",
					    text: "Please try again!"

					})
				}
				})
			}
		})
	}

	useEffect(()=>{

		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo.length === 11 && password1 !== "" && password2 !== "") && (password1 === password2)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	},[firstName, lastName, email, mobileNo, password1, password2])

	console.log(isActive)
	return(

		(user.id !== null)?
		<Navigate to="/products"/>
		:

	<>	
		<div className="
			border
			rounded
			py-3
			p-5
			mt-4 
			col-xs-12
			col-sm-10
			col-md-8
			col-lg-6
			col-xl-4
			offset-sm-1
			offset-md-2
			offset-lg-3 
			offset-xl-4" 
			id="regDiv">
		<Form onSubmit={(e)=>regUser(e)}>

		<img 
			src={img} 
			alt=""
			className="d-block img-fluid pt-4 px-0" 
		/>

		<h5 style={{
			    display: "flex",
			    justifyContent: "center",
			    alignItems: "center"
			    }}
			className="mb-3 mt-3" 
			id="regUserFont">CREATE ACCOUNT</h5>

			 <Form.Group className="mb-3" class="regSmallFonts" controlId="firstName">
				<Form.Label>First Name</Form.Label>
		        <Form.Control 
		        	type="text" 
		        	placeholder="Enter First Name"
		        	value = {firstName}
		        	onChange = {e=>setFirstName(e.target.value)}
		        	required
					/>		     
		    </Form.Group>

		    <Form.Group className="mb-3" class="regSmallFonts" controlId="lastName">
				<Form.Label>Last Name</Form.Label>
		        <Form.Control 
		        	type="text" 
		        	placeholder="Enter Last Name" 
		        	value = {lastName}
		        	onChange = {e=>setLastName(e.target.value)}
		        	required
		        	/>
		    </Form.Group>

		    <Form.Group className="mb-3" class="regSmallFonts" controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
		        <Form.Control 
		        	type="email" 
		        	placeholder="Enter Email"
		        	value = {email}
		        	onChange = {e=>setEmail(e.target.value)}
		        	required
		        	/>
		    </Form.Group>

		    <Form.Group className="mb-3" class="regSmallFonts" controlId="mobileNo">
				<Form.Label>Mobile Number</Form.Label>
		        <Form.Control 
		        	type="text" 
		        	placeholder="Enter Mobile Number"
		        	value = {mobileNo}
		        	onChange = {e=>setMobileNo(e.target.value)}
		        	required
		        	/>		     
		    </Form.Group>

		    <Form.Group className="mb-3" class="regSmallFonts" controlId="password1">
				<Form.Label>Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	placeholder="Password"
		        	value = {password1}
		        	onChange = {e=>setPassword1(e.target.value)}
		        	required
		        	/>		     
		    </Form.Group>

		    <Form.Group class="regSmallFonts" controlId="password2">
				<Form.Label>Verify Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	placeholder="Verify Password"
		        	value = {password2}
		        	onChange = {e=>setPassword2(e.target.value)} 
		        	required
		        	/>		     
		    </Form.Group>


		    {isActive ?

		    	<Button variant="primary" type="submit" id="submitBtn" className="mt-3">
		    	    Submit
		    	</Button>

		    :

		    	<Button variant="secondary" type="submit" id="submitBtnDsb" className="mt-3" disabled>
		    	    Submit
		    	</Button>
		    }
		</Form>
		</div>
	</>
	)
}