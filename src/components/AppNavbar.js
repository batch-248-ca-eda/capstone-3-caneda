import { Navbar, Nav, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import logo from '../images/logo2.png';
import UserContext from '../UserContext'
import { useContext } from 'react'


export default function AppNavBar() {

	const { user} = useContext(UserContext);
  
  return (
    <Navbar collapseOnSelect expand="lg" bg="white" variant="light">
      <Container fluid className="px-xs-5 px-xl-5 mx-xl-5 px-lg-5 px-md-4">
        <Navbar.Brand href="#home">
        	<img
        	      alt=""
        	        src={logo}
        	        width="230"
        	        height="35"
        	        className="d-inline-block align-top"/>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            {(user.isAdmin)?
                <Nav.Link as={Link} to="/admin">Dashboard</Nav.Link>
                :
                <>
                <Nav.Link as={Link} to="/products">Products</Nav.Link>
                {(user.id !== null)?
                	<Nav.Link as={Link} to={`/carts/${user.id}`}>Cart</Nav.Link>
                	:
                	<Nav.Link ></Nav.Link>
                }
                </>
            }
        </Nav>
        <Nav>

            {(user.isAdmin)?
              <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
            :
              <>
              {(user.id !== null)?
            	  <>
            	    <Nav.Link as={Link} to="/orders/:userId">Orders</Nav.Link>
                  <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
            	  </>
              :
				        <>
					        <Nav.Link as={Link} to="/login">Login</Nav.Link>
					        <Nav.Link as={Link} to="/register">Register</Nav.Link>
				        </>
			        }
              </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

