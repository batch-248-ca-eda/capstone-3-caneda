import img from '../images/imgTop.jpg';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom'

export default function Login(){

	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [isActive, setIsActive] = useState(false);

	function authenticate(e){

		e.preventDefault();

		fetch('https://capstone-2-caneda.onrender.com/users/login',{
			method: 'POST',
			headers:{
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password1
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)

			if (typeof data.access !== "undefined"){
				localStorage.setItem('token',data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: "Login Successful!",
					type: "success",
					text: `Welcome to Easy Shop`
				})
			} else {

				Swal.fire({
					title: "Login Failed",
					type: "error",
					text: "Email or Password is Incorrect"
				})
			}
		})

		const retrieveUserDetails = (token) => {

			fetch('https://capstone-2-caneda.onrender.com/users/details',{
				headers:{
					Authorization: `Bearer ${token}`
				}
			})
			.then(res=>res.json())
			.then(data=>{
				console.log(data);

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			})
		}

		setEmail("");
		setPassword1("");

	}

	useEffect(()=>{

		if(email !== "" && password1 !== ""){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	},[email, password1])

	console.log(user)

	return(

	(user.id !== null) ?
	<Navigate to="/"/>

	:

		<>	
			<div className="border rounded py-3 col-xs-12 col-sm-10 offset-sm-1 col-md-8 col-lg-6 col-xl-4 offset-lg-3 offset-xl-4 offset-md-2 p-5 mt-4 mb-5" id="logInBG">

			<img 
				src={img} 
				alt=""
				className=" d-block img-fluid pt-4" 
				/>

			<Form onSubmit={(e)=>authenticate(e)}>

			<h5 style={{
				    display: "flex",
				    justifyContent: "center",
				    alignItems: "center"
				    }}
				className="mt-3 mb-3" 
				id="logUserFont">SIGN IN</h5>

			    <Form.Group className="mb-3" class="logSmallFonts" controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
			        <Form.Control 
			        	type="email" 
			        	placeholder="Enter Email"
			        	value = {email}
			        	onChange = {e=>setEmail(e.target.value)}
			        	required
			        	/>
			    </Form.Group>

			    <Form.Group className="mb-3" class="logSmallFonts" controlId="password1">
					<Form.Label>Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Password"
			        	value = {password1}
			        	onChange = {e=>setPassword1(e.target.value)}
			        	required
			        	/>		     
			    </Form.Group>

			    	{isActive ?

			    		<Button variant="success" type="submit" id="submitBtn">
			    	 		Submit
			    		</Button>

			    		:

			    		<Button variant="secondary" type="submit" id="submitBtnDsb" disabled>
			    	  	Submit
			    		</Button>

			    	}

			</Form>

			</div>
			
		</>
	)

}