import {Row, Col, Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard(props) {


const {breakpoint, productProp } = props;
const {name, description, price, _id, imgLink} = productProp;

  return(

  <Col xs={12} md={breakpoint} className="mt-4">
    <Card className="cardHighlight p-3">
      {/*<img 
        src={`${imgLink}`}
        alt=""
        variant="top"
        className=" d-block img-fluid pt-4" 
        />*/}
      <Card.Img variant="top" fluid="true" src={`${imgLink}`}/>
      <Card.Body>
        <Card.Title>{name}</Card.Title>

        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>Php {price}</Card.Text>
        <Button id="prodViewBtn" as={Link} to={`/products/${_id}`}>Details</Button>
      </Card.Body>
    </Card>
  </Col>
  )
  
}


{/**/}

   {/* 
      <Card>
        
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Description: {description}</Card.Subtitle>
          <Card.Subtitle>Price: {price}</Card.Subtitle>
          <Button className="bg-primary" as={Link} to={`/products/${_id}`}>Details</Button>
        </Card.Body>
      </Card>        
    */}