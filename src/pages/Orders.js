import { useContext, useState, useEffect } from "react";
import UserContext from "../UserContext";
import { Container } from 'react-bootstrap';

export default function Orders(){

	const {user} = useContext(UserContext);
	const [ orders, setOrders ] = useState("");
	// const [ ordInfo, setOrdInfo ] = useState("")






	const fetchOrders = () => {
		fetch(`https://capstone-2-caneda.onrender.com/orders/getOrder/${user.id}`,{
			headers: {
				"Content-Type": "application/json"
			}
		})
		.then(res=>res.json())
		.then(data=>{

			// setOrders(data.map((orders,index)=>{
			// console.log(orders)
			return (
				<>
				<div className="bg-white">
					<div>{data}</div>
					{/*<div>{orders.totalAmount}</div>
					<div>{orders.quantity}</div>
					<div>{orders.purchasedOn}</div>
					<div>{orders.productsInCart}</div>*/}
				</div>
				</>

			)
			// }))
		})
	}


	useEffect(()=>{

		fetchOrders();

	},[])


	return (

		<Container key={orders._id}>
			<div key={orders._id}>
				{orders}
			</div>
		</Container>

	)

}