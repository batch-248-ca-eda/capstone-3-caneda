import { useContext, useState, useEffect } from "react";
import UserContext from "../UserContext";
import { Button, Col, Row, Container, Image, Card} from "react-bootstrap"
import Swal from "sweetalert2";
import empty from '../images/empty.png';
import { Link } from 'react-router-dom';

export default function Dash(){

	const {user} = useContext(UserContext);

	const [ carts, setCarts ] = useState([]);
	const [ cartTotal, setCartTotal ] = useState("");

	

	const fetchCart = () => {
		fetch(`https://capstone-2-caneda.onrender.com/carts/${user.id}`)
		.then(res=>res.json())
		.then(data=>{
			// console.log(data)
			if (data === false) {
				 
				setCarts(false)
			
			} else {

				setCarts(data.map((cart, pos) => {
					// console.log(cart.productName)
				
					return (

						<>
							<Row key={pos} id="prodView">
							    <Col xl="4" lg="4" md="4" sm="12" className="" key={pos}><Image id="imgCart"src={cart.imgLink} className="img-fluid" /></Col>
							    <Col xl="5" lg="5" md="5" sm="8" xs="7" className="mx-4" key={cart.productName}>
							      <Row className="mt-5 row-md-12" key={cart.productName}>
							        {cart.productName}
							      </Row>
							      <Row key={cart.productId}>
							        ID: {cart.productId}
							      </Row>
							      <Row className="d-flex ">
							        Quantity: {cart.prodQuantity}
							        {/*<div class="input-group" className="d-flex" key={cart.prodQuantity}>
							            <div class="input-group-prepend">
							                <button class="btn" id="incBtn" type="button" onClick={decNum}>-</button>
							            </div>
							            <input type="text" class="form-control" id="quanty" defaultValue= onChange={handleChange}/>
							            <div class="input-group-prepend">
							              <button class="btn" id="incBtn" type="button" onClick={incNum}>+</button>
							            </div>
							        </div>*/}
							      </Row>
							      <Row key={cart.subTota}>
							        Sub Total: {cart.subTotal}
							      </Row>
							    </Col>
							    <Col xs lg="2"  className="
							    	mt-5 
							    	d-block
							    	col-md-2
							    	">
							      <Button id="cartBtn" variant="danger" onClick ={() => cartDelProd(cart._id, cart.subTotal, cart.prodQuantity)}>Delete</Button>
							    </Col>
							</Row>
						</>
					)
				}))
			} 
			
		})
	}//fetchcart END
	

	const fetchTotal = () => {
		fetch(`https://capstone-2-caneda.onrender.com/carts/viewUserCart/${user.id}`,{
			headers:{
				"Content-Type": "application/json"
			}
		})
		.then(res => res.json())
		.then(data => {
			
			setCartTotal(data.map(cartPrice=>{
				// console.log(cartPrice._id)
				return (
					<>
					<div key={cartPrice.total}>Cart Total: ₱{cartPrice.total}</div>
					<div key={cartPrice.quantity}>Quantity: {cartPrice.quantity}</div>
					</>
				)
			}))
			// fetchTotal();
		})
			
	}


	const checkOut = () => {
		fetch(`https://capstone-2-caneda.onrender.com/orders/createOrder`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				userId: user.id
			})
		})
		.then(res2 => res2.json())
		.then(data2 => {

			if (data2){
				Swal.fire({
				    title: "Order Successful",
				    type: "success",
				    text: `All products from cart has been ordered`
				});
				fetchCart();
				fetchTotal();
			} else {

				Swal.fire({
				    title: "Error!",
				    type: "error",
				    text: `Something went wrong. Please try again later!`
				});
			}
		})
	}


	const cartDelProd = (arrayId, subTotal, prodSubQuan) => {
		// console.log(arrayId)
		
		fetch(`https://capstone-2-caneda.onrender.com/carts/deleteFromCart`, {
			method: "DELETE",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				userId: user.id,
				arrayId: arrayId,
				amountToDeduct: subTotal,
				prodQuantity: prodSubQuan
			})
		})
		.then(res1 => res1.json())
		.then(data1 => {
			// console.log(data1)
			if(data1){
				Swal.fire({
				    title: "Removed",
				    type: "success",
				    text: `Product removed from cart`
				});
				fetchCart();
				fetchTotal();

				
			}
			else{
				Swal.fire({
				    title: "Error!",
				    type: "error",
				    text: `Something went wrong. Please try again later!`
				});
			}

		})
	}

	useEffect(()=>{

		// console.log(cartTotal._id)
		// console.log(carts)
		fetchCart();
		fetchTotal();

		
	},[])


	return (
		(carts === false || carts === [])?
		<Container fluid className ="mt-5" >
	  <Row >
	    <Col xl={{span:4, offset:4}} lg={{span:7, offset:2}} md={{span:8, offset:2}}> 
	      <Card className="cardHighlight img-fluid p-3">
	      	<Card.Img variant="top" className="d-block mx-auto img-fluid" src={empty}/>
	        <Card.Body class="text-center">
	          <Card.Title class="text-center" className="mb-4"><h5>Looks like you have not added anything to your cart. Go ahead and explore product catalogue to find products</h5></Card.Title>
	          <Button as={Link} to="/products" id="prodBtn">Products</Button>
	        </Card.Body>
	      </Card>
	    </Col>
	  </Row>
	 </Container>

		:
		<Container className="mt-5">
			<Container className="
				bg-white 
				col-xl-10
				col-lg-9
				col-md-12
				col-sm-12
				offset-xl-
				">
				{carts}
				<div className="bg-secondary d-flex" key={cartTotal.total} class="text-center">
					<h3 className="mt-3">{cartTotal}</h3>
				 	<button class="btn" id="checkOutBtn" type="button" onClick={()=>checkOut()}>Check Out</button>
				</div>
			</Container>
		</Container>
		
	)

}