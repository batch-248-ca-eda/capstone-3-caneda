import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext'
import ProductCard from '../components/ProductCard';
import {Row, Container} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';


export default function Products(){

	const [products, setProducts] = useState([]);

	const {user} = useContext(UserContext);

	useEffect(()=>{

		fetch(`https://capstone-2-caneda.onrender.com/products`)
		.then(res=>res.json())
		.then(data=>{

			const productArr = (data.map(product=>{

				return (
					<ProductCard productProp={product} key={product._id} breakpoint={4}/>
				)

			}))
			setProducts(productArr)
		})
	},[])

	return(

		(user.isAdmin)?
		<Navigate to="/admin"/>
		:
		<>
			<Container id="prodContainter">
			<h1 className="
				mt-4
				p-2 
				text-center 
				col-xl-4
				offset-xl-4"

				id="prodLabel">Product Selection</h1>
			<Row>
			{products}
			</Row>
			</Container>
		</>
	)
}