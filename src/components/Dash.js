import { useContext, useState, useEffect } from "react";
import {Table, Button, Col, Row, Nav, Tab, Form, Container} from "react-bootstrap";
import {Navigate, Link, useNavigate} from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import edit from '../images/edit.png';

export default function Dash(){

	const {user} = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([]);
	const [allUsers, setAllUsers] = useState([]);

	const fetchData = () =>{
		fetch(`https://capstone-2-caneda.onrender.com/products/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);

			setAllProducts(data.map(product => {
				return(
					<tr key={product._id} className="">
						<td className="text-center">{product._id}</td>
						<td className="text-center">{product.name}</td>
						<td className="text-center">{product.description}</td>
						<td className="text-center">{product.price}</td>
						<td className="text-center">{product.isActive ? "Active" : "Inactive"}</td>
						<td className="d-flex">
						<Form.Check
								type="switch"
								checked={product.isActive}
								onChange={() => product.isActive ? archive(product._id, product.name): unarchive(product._id, product.name)}
						/>
						<Button as={ Link } to={`/editProduct/${product._id}`} variant="secondary" size="sm" >Edit</Button>
						<Button  variant="danger" onClick={()=>deleteProd(product._id)} size="sm" className="mx-1">Delete</Button>
						</td>
					</tr>
				)
			}))

		})
	}



	const fetchUser = () => {
		fetch(`https://capstone-2-caneda.onrender.com/users/all`,{
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res1 => res1.json())
		.then(data1 => {
			console.log(data1);

			setAllUsers(data1.map(user => {
				console.log(user)
				return(
					<tr key={user._id} className="">
						<td className="text-center">{user._id}</td>
						<td className="text-center">{user.firstName}</td>
						<td className="text-center">{user.lastName}</td>
						<td className="text-center">{user.email}</td>
						<td className="text-center">{user.mobileNo}</td>
						<td className="text-center">{user.isAdmin ? "Admin" : "Not an Admin"}</td>
						<td className="d-flex text-center">
						<Form.Check
								type="switch"
								checked={user.isAdmin}
								onChange={() => user.isAdmin ? setNonAdmin(user._id, user.firstName): setToAdmin(user._id, user.firstName)}
						/>
						<Button  variant="danger" onClick={()=>deleteUser(user._id)} size="sm" className="mx-1">Delete</Button>
						</td>
					</tr>
				)
			}))

		})
	}


	const deleteUser = (userId) => {
		fetch(`https://capstone-2-caneda.onrender.com/users/delete`,{
			method: "DELETE",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				userId: userId
			})
		})
		.then(delUse => delUse.json())
		.then(delUser => {

			if (delUser) {

				Swal.fire({
				    title: "User Deleted",
				    type: "success",
				    text: `User has been removed from database`
				});
				fetchUser();
			} else {

				Swal.fire({
				    title: "Error!",
				    type: "error",
				    text: `Something went wrong. Please try again later!`
				});
			}
		})
	}


	const deleteProd = (productId) => {
		fetch(`https://capstone-2-caneda.onrender.com/products/delete`,{
			method: "DELETE",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId:productId
			})
		})
		.then(del => del.json())
		.then(delData => {

			if (delData) {

				Swal.fire({
				    title: "Product Deleted",
				    type: "success",
				    text: `Product has been removed from database`
				});
				fetchData();
			} else {

				Swal.fire({
				    title: "Error!",
				    type: "error",
				    text: `Something went wrong. Please try again later!`
				});
			}
		})
	}


	const setNonAdmin = (userId, name) =>{
		/*console.log(userId);
		console.log(name)*/
		fetch(`https://capstone-2-caneda.onrender.com/users/updateToNonAdmin/${userId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAdmin: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			// console.log(data);

			if(data){
				Swal.fire({
					title: `${name} has been removed as an Admin`,
					type: "success",
					text: `${name} is now a Non Admin.`
				})
				fetchUser();
			}
			else{
				Swal.fire({
					title: "Product Archive Unsuccessful",
					type: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}


	const setToAdmin = (userId, name) =>{
		/*console.log(userId);
		console.log(name)*/
		fetch(`https://capstone-2-caneda.onrender.com/users/updateToAdmin/${userId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAdmin: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			// console.log(data);

			if(data){
				Swal.fire({
					title: `${name} is set to Admin`,
					type: "success",
					text: `${name} is now an Admin.`
				})
				fetchUser();
			}
			else{
				Swal.fire({
					title: "Product Archive Unsuccessful",
					type: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}



	const archive = (productId, productName) =>{
		/*console.log(productId);
		console.log(productName);*/

		fetch(`https://capstone-2-caneda.onrender.com/products/archiveProduct/${productId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			// console.log(data);

			if(data){
				Swal.fire({
					title: "Product Deactivated!",
					type: "success",
					text: `${productName} is now inactive.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Product Archive Unsuccessful",
					type: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	const unarchive = (productId, productName) =>{
		/*console.log(productId);
		console.log(productName);*/

		fetch(`https://capstone-2-caneda.onrender.com/products/activateProduct/${productId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			// console.log(data);

			if(data){
				Swal.fire({
					title: "Product Actived!",
					type: "success",
					text: `${productName} is now active.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					type: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}



		const navigate = useNavigate();

		const [name, setName] = useState('');
		const [description, setDescription] = useState('');
		const [price, setPrice] = useState(0);
		const [imgLink, setImgLink] = useState('');

	    const [isActive, setIsActive] = useState(false);

		function addProduct(e) {

		    e.preventDefault();
		    /*console.log(name)
		    console.log(description)
		    console.log(price)
		    console.log(imgLink)*/
		    fetch(`https://capstone-2-caneda.onrender.com/products`, {
		    	method: "POST",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
				    name: name,
				    description: description,
				    price: price,
				    imgLink: imgLink

				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Product succesfully Added",
		    		    type: "success",
		    		    text: `${name} is now added`
		    		});
		    		fetchData();

		    		navigate("/admin");
		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    type: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    	}

		    })

		    setName('');
		    setDescription('');
		    setPrice(0);
		    setImgLink('')

		}


		useEffect(() => {
			// console.log(allUsers)
			fetchData();
			fetchUser();

	        if(name !== "" && description !== "" && price > 0 && imgLink !==""){
	            setIsActive(true);
	        } else {
	            setIsActive(false);
	        }

	    }, [name, description, price, imgLink]);


	return(
		(user.isAdmin)
		?
		<>
			<Container fluid>
			<div className="mt-4 mb-3 text-center offset-xl-2 offset-lg-2 offset-md-4" >
				<h1>Admin Dashboard</h1>
			</div>
			<Tab.Container class="mt-5" id="left-tabs-example" defaultActiveKey="first" xs={12} md={12}>
				<Row>
					<Col sm={2}>
						<Nav variant="pills" className="flex-column bg-light">
							<Nav.Item>
			 			        <Nav.Link eventKey="first" >All Products</Nav.Link>
			 			    </Nav.Item>
			 			    <Nav.Item>
			 			        <Nav.Link eventKey="second">All Users</Nav.Link>
			 			    </Nav.Item>
			 			    <Nav.Item>
			 			        <Nav.Link eventKey="third">Add Product</Nav.Link>
			 			    </Nav.Item>
			 			</Nav>
			 		</Col>
			 		<Col sm={10}>
			 			<Tab.Content>
			 			    <Tab.Pane eventKey="first">
			 			        <Table striped hover variant="light">
			 			            <thead>
			 			             	<tr>
			 			             		<th className="text-center" scope="col">Product ID</th>
			 			             		<th className="text-center" scope="col">Product Name</th>
			 			             		<th className="text-center" scope="col">Description</th>
			 			             		<th className="text-center" scope="col">Price</th>
			 			             		<th className="text-center" scope="col">Status</th>
			 			             		<th className="text-center" scope="col">Action</th>
			 			             		         
			 			             	</tr>
			 			            </thead>
			 			            <tbody>
			 			             	{ allProducts }
			 			            </tbody>
			 			        </Table>
			 			    </Tab.Pane>
			 			    <Tab.Pane eventKey="second">
			 			        <Table striped hover variant="light">
			 			            <thead>
			 			             	<tr>
			 			             		<th className="text-center" scope="col">User ID</th>
			 			             		<th className="text-center" scope="col">First Name</th>
			 			             		<th className="text-center" scope="col">Last Name</th>
			 			             		<th className="text-center" scope="col">Email</th>
			 			             		<th className="text-center" scope="col">Mobile No.</th>
			 			             		<th className="text-center" scope="col">is Admin</th>
			 			             		<th className="text-center" scope="col">Action</th>
			 			             	</tr>
			 			            </thead>
			 			            <tbody>
			 			             	{ allUsers }
			 			            </tbody>
			 			        </Table>
			 			    </Tab.Pane>
			 			    <Tab.Pane eventKey="third" className="bg-light">
			 			    	<Container className="p-3" rounded>
			 			        	<Form onSubmit={(e) => addProduct(e)}>
			 			        	    <Form.Group controlId="name" className="mb-3">
			 			        	        <h5>Product Name</h5>
			 			        	        <Form.Control 
			 			        		        type="text" 
			 			        		        placeholder="Enter Product Name" 
			 			        		        value = {name}
			 			        		        onChange={e => setName(e.target.value)}
			 			        		        required
			 			        	            />
			 			        	    </Form.Group>
			 			        	    <Form.Group controlId="description" className="mb-3">
			 			        	        <h5>Product Description</h5>
			 			        	        <Form.Control
			 			        	            as="textarea"
			 			        	            rows={3}
			 			        		        placeholder="Enter Product Description" 
			 			        		        value = {description}
			 			        		        onChange={e => setDescription(e.target.value)}
			 			        		        required
			 			        	            />
			 			        	    </Form.Group>
			 			        	    <Form.Group controlId="price" className="mb-3">
			 			        	     	<h5>Product Price</h5>
			 			        	        <Form.Control 
			 			        		        type="number" 
			 			        		        placeholder="Enter Product Price" 
			 			        		        value = {price}
			 			        		        onChange={e => setPrice(e.target.value)}
			 			        		        required
			 			        	            />
			 			        	    </Form.Group>
			 			        	    <Form.Group controlId="price" className="mb-3">
			 			        	        <h5>Product Image Link</h5>
			 			        	        <Form.Control 
			 			        	    	    type="text" 
			 			        	    	    placeholder="Enter Product Image Link" 
			 			        	    	    value = {imgLink}
			 			        	    	    onChange={e => setImgLink(e.target.value)}
			 			        	    	    required
			 			        	            />
			 			        	    </Form.Group>

			 			                { isActive ? 
			 			                	<Button variant="secondary" type="submit" id="submitBtn1s">
			 			                	    Save
			 			                	</Button>
			 			                : 
			 			                	<Button variant="secondary" type="submit" id="submitBtnDsb1s" disabled>
			 			                	    Save
			 			                	</Button>
			 			                }
			 			                	<Button className="m-2" as={Link} to="/admin" variant="secondary" type="submit" id="submitBtn">
			 			                	    Cancel
			 			                	</Button>
			 			        	</Form>
			 			       	</Container>
			 			    </Tab.Pane>
			 			</Tab.Content>
			 		</Col>
			 	</Row>
			</Tab.Container>
			</Container>
		</>
		:
		<Navigate to="/products" />
	)
}


