import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import { Form, Button, Container } from 'react-bootstrap';

export default function EditProduct() {

	const {user} = useContext(UserContext);

	const { productId } = useParams();

	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [imgLink, setImgLink] = useState('');

    const [isActive, setIsActive] = useState(false);

	function editProduct(e) {

	    e.preventDefault();

	    fetch(`https://capstone-2-caneda.onrender.com/products/updateProduct/${productId}`, {
	    	method: "PUT",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
			    name: name,
			    description: description,
			    price: price,
			    imgLink: imgLink			
			})
	    })
	    .then(res => res.json())
	    .then(data => {
	    	console.log(data);

	    	if(data){
	    		Swal.fire({
	    		    title: "Product succesfully Updated",
	    		    type: "success",
	    		    text: `${name} is now updated`
	    		});

	    		navigate("/admin");
	    	}
	    	else{
	    		Swal.fire({
	    		    title: "Error!",
	    		    type: "error",
	    		    text: `Something went wrong. Please try again later!`
	    		});
	    	}

	    })

	    // Clear input fields
	    setName('');
	    setDescription('');
	    setPrice(0);
	    setImgLink('');

	}

	// Submit button validation
	useEffect(() => {

        // Validation to enable submit button when all fields are populated and set a price and slot greater than zero.
        if(name !== "" && description !== "" && price > 0 ){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description, price]);

    useEffect(()=> {

    	console.log(productId);

    	fetch(`https://capstone-2-caneda.onrender.com/products/${productId}`)
    	.then(res => res.json())
    	.then(data => {

    		console.log(data);

    		setName(data.name);
    		setDescription(data.description);
    		setPrice(data.price);
    		setImgLink(data.imgLink)

    	});

    }, [productId]);

    return (
    	user.isAdmin
    	?
			<>
			<Container className="bg-light">
		    	<h1 className="my-5 text-center pt-4">Edit Product</h1>
		        <Form onSubmit={(e) => editProduct(e)}>
		        	<Form.Group controlId="name" className="mb-3">
		                <Form.Label>Product Name</Form.Label>
		                <Form.Control 
			                type="text" 
			                value = {name}
			                onChange={e => setName(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="description" className="mb-3">
		                <Form.Label>Product Description</Form.Label>
		                <Form.Control
		                	as="textarea"
		                	rows={3}
			                value = {description}
			                onChange={e => setDescription(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="price" className="mb-3">
		                <Form.Label>Product Price</Form.Label>
		                <Form.Control 
			                type="number" 
			                value = {price}
			                onChange={e => setPrice(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="imglink" className="mb-3">
		                <Form.Label>Product Image link</Form.Label>
		                <Form.Control 
			                type="text" 
			                value = {imgLink}
			                onChange={e => setImgLink(e.target.value)}
			                required
		                />
		            </Form.Group>

		           
	        	    { isActive 
	        	    	? 
	        	    	<Button variant="primary" type="submit" id="submitBtnss">
	        	    		Update
	        	    	</Button>
	        	        : 
	        	        <Button variant="danger" type="submit" id="submitBtn" disabled>
	        	        	Update
	        	        </Button>
	        	    }
	        	    	<Button className="m-2" as={Link} to="/admin" variant="success" type="submit" id="submitBtn">
	        	    		Cancel
	        	    	</Button>
		        </Form>
		    </Container>
	    	</>
    	:
    	    <Navigate to="/products" />
	    	
    )

}
