import { useContext, useState, useEffect } from "react";
import {Table, Button, Col, Row, Nav, Tab, Form} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import ReactSwitch from "react-switch";

export default function Dash(){

	const {user} = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([]);
	const [allUsers, setAllUsers] = useState([]);

	const fetchData = () =>{
		fetch(`http://localhost:5000/products/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data);

			setAllProducts(data.map(product => {
				return(
					<tr key={product._id} className="">
						<td className="text-center" scope="col">{product._id}</td>
						<td className="text-center" scope="col">{product.name}</td>
						<td className="text-center" scope="col">{product.description}</td>
						<td className="text-center" scope="col">{product.price}</td>
						<td className="text-center" scope="col">{product.isActive ? "Active" : "Inactive"}</td>
						<td className="d-flex">
						<Form.Check
								type="switch"
								checked={product.isActive}
								onChange={() => product.isActive ? archive(product._id, product.name): unarchive(product._id, product.name)}
						/>
						<Button as={ Link } to={`/editProduct/${product._id}`} variant="secondary" size="sm" >Edit</Button>
						</td>
					</tr>
				)
			}))

		})
	}

	const fetchUser = () =>{
		fetch(`http://localhost:5000/users/all`,{
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res1 => res1.json())
		.then(data1 => {
			// console.log(data);

			setAllUsers(data1.map(user => {
				return(
					<tr key={user._id} className="">
						<td>{user._id}</td>
						<td>{user.firstName}</td>
						<td>{user.lastName}</td>
						<td>{user.email}</td>
						<td>{user.mobileNo}</td>
						<td>{user.isAdmin ? "Admin" : "Not an Admin"}</td>
						<td></td>
					</tr>
				)
			}))

		})
	}

	const archive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`http://localhost:5000/products/archiveProduct/${productId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			// console.log(data);

			if(data){
				Swal.fire({
					title: "Product Deactivated!",
					type: "success",
					text: `${productName} is now inactive.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Product Archive Unsuccessful",
					type: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	const unarchive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`http://localhost:5000/products/activateProduct/${productId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			// console.log(data);

			if(data){
				Swal.fire({
					title: "Product Actived!",
					type: "success",
					text: `${productName} is now active.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					type: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	useEffect(()=>{

		fetchData();
	})

	useEffect(()=>{

		fetchUser();
	})

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5" >
			 <Tab.Container className="mt-5 bg-dark" id="left-tabs-example" defaultActiveKey="first" xs={12}>
			 			     <Row>
			 			       <Col sm={3}>
			 			         <Nav variant="pills" className="flex-column">
			 			           <Nav.Item>
			 			             <Nav.Link eventKey="first">All Products</Nav.Link>
			 			           </Nav.Item>
			 			           <Nav.Item>
			 			             <Nav.Link eventKey="second">All Users</Nav.Link>
			 			           </Nav.Item>
			 			         </Nav>
			 			       </Col>
			 			       <Col sm={9}>
			 			         <Tab.Content>
			 			           <Tab.Pane eventKey="first">
			 			             <div className="mb-3 text-center">
			 			             				<h1>Admin Dashboard</h1>
			 			             				<Button as={Link} to="/addProduct" variant="light" size="lg" className="mx-2">Add Product</Button>
			 			             			</div>
			 			             			<Table striped hover variant="light">
			 			             		     <thead>
			 			             		       <tr>
			 			             		         <th className="text-center" scope="col">Product ID</th>
			 			             		         <th className="text-center" scope="col">Product Name</th>
			 			             		         <th className="text-center" scope="col">Description</th>
			 			             		         <th className="text-center" scope="col">Price</th>
			 			             		         <th className="text-center" scope="col">Status</th>
			 			             		         <th className="text-center" scope="col">Action</th>
			 			             		         
			 			             		       </tr>
			 			             		     </thead>
			 			             		     <tbody>
			 			             		       { allProducts }
			 			             		     </tbody>
			 			             		   </Table>

			 			           </Tab.Pane>
			 			           <Tab.Pane eventKey="second">
			 			             <div className="mb-3 text-center">
			 			             				<h1>Admin Dashboard</h1>
			 			             			</div>
			 			             			<Table striped hover variant="light">
			 			             		     <thead>
			 			             		       <tr>
			 			             		         <th>User ID</th>
			 			             		         <th>First Name</th>
			 			             		         <th>Last Name</th>
			 			             		         <th>Email</th>
			 			             		         <th>Mobile No.</th>
			 			             		         <th className="text-center">is Admin</th>
			 			             		       </tr>
			 			             		     </thead>
			 			             		     <tbody>
			 			             		       { allUsers }
			 			             		     </tbody>
			 			             		   </Table>

			 			           </Tab.Pane>
			 			         </Tab.Content>
			 			       </Col>
			 			     </Row>
			 			   </Tab.Container>
			 			  </div>
		</>
		:
		<Navigate to="/products" />
	)
}
























import { useContext, useState, useEffect } from "react";
import {Table, Button, Col, Row, Nav, Tab, Form} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import ReactSwitch from "react-switch";

export default function Dash(){

	const {user} = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([]);
	const [allUsers, setAllUsers] = useState([]);

	
		
		useEffect(() => {

			Promise.all([
			      fetch('http://localhost:5000/products/all',{
			      	headers:{
			      		"Authorization": `Bearer ${localStorage.getItem("token")}`
			      	}
			      }),
			      fetch('http://localhost:5000/users/all',{
			      	headers:{
			      		"Authorization": `Bearer ${localStorage.getItem("token")}`
			      	}
			      })
			    ])
				.then(([resProducts, resUsers]) =>
					Promise.all([resProducts.json(), resUsers.json()])
				)
				.then(([dataProducts, dataUsers]) => {
					setAllUsers(dataUsers.map(user => {
						return(
							<tr key={user._id} className="">
								<td>{user._id}</td>
								<td>{user.firstName}</td>
								<td>{user.lastName}</td>
								<td>{user.email}</td>
								<td>{user.mobileNo}</td>
								<td>{user.isAdmin ? "Admin" : "Not an Admin"}</td>
								<td></td>
							</tr>
						)
					}))

					setAllProducts(dataProducts.map(product => {
						return(
							<tr key={product._id} className="">
								<td className="text-center" scope="col">{product._id}</td>
								<td className="text-center" scope="col">{product.name}</td>
								<td className="text-center" scope="col">{product.description}</td>
								<td className="text-center" scope="col">{product.price}</td>
								<td className="text-center" scope="col">{product.isActive ? "Active" : "Inactive"}</td>
								<td className="d-flex">
								<Form.Check
										type="switch"
										checked={product.isActive}
										onChange={() => product.isActive ? archive(product._id, product.name): unarchive(product._id, product.name)}
								/>
								<Button as={ Link } to={`/editProduct/${product._id}`} variant="secondary" size="sm" >Edit</Button>
								</td>
							</tr>
						)
					}))
					
				})
		}, []);	

	const archive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);
		
		fetch(`http://localhost:5000/products/archiveProduct/${productId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			// console.log(data);

			if(data){
				Swal.fire({
					title: "Product Deactivated!",
					type: "success",
					text: `${productName} is now inactive.`
				})
				// fetchData();
			}
			else{
				Swal.fire({
					title: "Product Archive Unsuccessful",
					type: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	const unarchive = (productId, productName) =>{
		console.log(productId);
		console.log(productName);

		fetch(`http://localhost:5000/products/activateProduct/${productId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			// console.log(data);

			if(data){
				Swal.fire({
					title: "Product Actived!",
					type: "success",
					text: `${productName} is now active.`
				})
				// fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					type: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}



	return(

		(user.isAdmin)
		?
		<>
			<div className="mt-5" >
			 <Tab.Container className="mt-5 bg-dark" id="left-tabs-example" defaultActiveKey="first" xs={12}>
			 			     <Row>
			 			       <Col sm={3}>
			 			         <Nav variant="pills" className="flex-column">
			 			           <Nav.Item>
			 			             <Nav.Link eventKey="first">All Products</Nav.Link>
			 			           </Nav.Item>
			 			           <Nav.Item>
			 			             <Nav.Link eventKey="second">All Users</Nav.Link>
			 			           </Nav.Item>
			 			         </Nav>
			 			       </Col>
			 			       <Col sm={9}>
			 			         <Tab.Content>
			 			           <Tab.Pane eventKey="first">
			 			             <div className="mb-3 text-center">
			 			             				<h1>Admin Dashboard</h1>
			 			             				<Button as={Link} to="/addProduct" variant="light" size="lg" className="mx-2">Add Product</Button>
			 			             			</div>
			 			             			<Table striped hover variant="light">
			 			             		     <thead>
			 			             		       <tr>
			 			             		         <th className="text-center" scope="col">Product ID</th>
			 			             		         <th className="text-center" scope="col">Product Name</th>
			 			             		         <th className="text-center" scope="col">Description</th>
			 			             		         <th className="text-center" scope="col">Price</th>
			 			             		         <th className="text-center" scope="col">Status</th>
			 			             		         <th className="text-center" scope="col">Action</th>
			 			             		         
			 			             		       </tr>
			 			             		     </thead>
			 			             		     <tbody>
			 			             		       { allProducts }
			 			             		     </tbody>
			 			             		   </Table>

			 			           </Tab.Pane>
			 			           <Tab.Pane eventKey="second">
			 			             <div className="mb-3 text-center">
			 			             				<h1>Admin Dashboard</h1>
			 			             			</div>
			 			             			<Table striped hover variant="light">
			 			             		     <thead>
			 			             		       <tr>
			 			             		         <th>User ID</th>
			 			             		         <th>First Name</th>
			 			             		         <th>Last Name</th>
			 			             		         <th>Email</th>
			 			             		         <th>Mobile No.</th>
			 			             		         <th className="text-center">is Admin</th>
			 			             		       </tr>
			 			             		     </thead>
			 			             		     <tbody>
			 			             		       { allUsers }
			 			             		     </tbody>
			 			             		   </Table>

			 			           </Tab.Pane>
			 			         </Tab.Content>
			 			       </Col>
			 			     </Row>
			 			   </Tab.Container>
			 			  </div>
		</>
		:
		<Navigate to="/products" />
	)
}

