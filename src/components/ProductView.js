import { useState, useEffect, useContext } from 'react';
import {Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView(){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const {productId} = useParams();


	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [imgLink, setImgLink] = useState("");



	const addToCart = (productId) => {
		fetch(`https://capstone-2-caneda.onrender.com/carts/${productId}`,{
			method: "POST",
			headers: {
				"Content-Type":"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body:JSON.stringify({
				productId: productId,
				userId: user.id,
				quantity: num,
				imgLink: imgLink,
				productName: name
			})
		})
		.then(res=>res.json())
		.then(data=>{
			// console.log(data)

			if (data===true){

				Swal.fire({
					title:"Product added to cart!",
					type:"success",
					text:"View your cart to see the product in your cart"
				})

				navigate("/products")
			} else {

				Swal.fire({
					title:"Something went wrong",
					type:"error",
					text:"Please try again."
				})
			}
		})
	}


	
	  let [num, setNum]= useState(1);
	  let incNum =()=>{
	    if(num<1000)
	    {
	    setNum(Number(num)+1);
	    }
	  };
	  let decNum = () => {
	     if(num>0)
	     {
	      setNum(num - 1);
	     }
	  }
	 let handleChange = (e)=>{
	   setNum(e.target.value);
	  }



	useEffect(()=>{

		// console.log(user.id)
		// console.log(productId)

		fetch(`https://capstone-2-caneda.onrender.com/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setImgLink(data.imgLink)

		})
	},[])



	return (
	<Container fluid className ="mt-5">
	  <Row >
	    <Col xl={{span:4, offset:4}} lg={{span:7, offset:2}} md={{span:8, offset:2}}> 
	      <Card className="cardHighlight img-fluid p-3">
	      	<Card.Img variant="top" className="d-block mx-auto img-fluid" src={`${imgLink}`}/>
	        <Card.Body>
	          <Card.Title className="mb-3">{name}</Card.Title>

	          <Card.Text>
	            {description}
	          </Card.Text>
	          <Card.Subtitle>Price</Card.Subtitle>
	          <Card.Text>
	            Php {price}
	          </Card.Text>

	          {	
	          	(user.id !== null)?
				<>
				<div className="
					col-xl-12
					col-lg-12
					">
				    <div class="input-group" className="d-flex">
				  		<div class="input-group-prepend">
				    		<button class="btn" id="incBtn" type="button" onClick={decNum}>-</button>
				  		</div>
				  		<input type="text" class="form-control" id="quant" value={num} onChange={handleChange}/>
				  		<div class="input-group-prepend">
				    		<button class="btn" id="incBtn" type="button" onClick={incNum}>+</button>
				  		</div>
				  		<Button id="addToCartBtn" className="mx-2" onClick={()=>addToCart(productId)}>Add to Cart</Button>
					</div>
				</div>
	          	
	          	</>
	          	:
	          	<Link className="btn btn-danger" to="/login">Log in to add to cart</Link>
	          	
	          }

	          
	        </Card.Body>
	      </Card>
	    </Col>
	  </Row>
	 </Container>
	)





}