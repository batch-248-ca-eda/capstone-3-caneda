import { Carousel, Container, Row, Col } from 'react-bootstrap';
import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext'



export default function Home(){


	const [products, setProducts] = useState([]);

	const {user} = useContext(UserContext);

	useEffect(()=>{

		fetch(`https://capstone-2-caneda.onrender.com/products`)
		.then(res=>res.json())
		.then(data=>{

			const productArr = (data.map(product=>{

				return (
					<Carousel.Item class="text-center" >
					<img
		          		className="
		          			d-block 
		          			img-fluid
		          			col-xs-1
		          			text-center
		          			"
		         		src={product.imgLink}
		          		alt="First slide"
		          		id="homeImg"
		        		/>
		        	
		        	    <h1 className="text-dark" id="homeCap">{product.name}</h1>
		        	
		        	</Carousel.Item>
				)

			}))
			setProducts(productArr)
		})
	},[])




	return (
	
		<Container fluid className="
						border 
						rounded
						mt-5
						bg-white 
						col-md-9
						col-xl-9
						col-xs-flex
						" >

			<Container className="col-xl-6 ">

			<Row>
        		<Col>
        			<Carousel>
						{products}
		    		</Carousel>
		    	</Col>
      		</Row>
      		</Container>
		</Container>



	)
}